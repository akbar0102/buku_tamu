package com.example.springboot.bukutamu.repository;

import java.util.List;

import com.example.springboot.bukutamu.model.Tamu;

public interface TamuDAO {
	public List<Tamu> findAll();
	
	public void save(Tamu tamu);
	
	public Tamu findById(int id);
	
	public void deleteById(int id);
}
