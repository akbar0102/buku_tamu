package com.example.springboot.bukutamu.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.springboot.bukutamu.model.Tamu;

@Repository
public class TamuDAOImpl implements TamuDAO {
	
	private EntityManager entityManager;
	
	@Autowired
	public TamuDAOImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	public List<Tamu> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<Tamu> theQuery = currentSession.createQuery("from Tamu", Tamu.class);
		
		// execute query and get result list
		List<Tamu> guests = theQuery.getResultList();
		
		// return the result 
		return guests;
	}

	@Override
	public void save(Tamu tamu) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		
		//save employee
		currentSession.saveOrUpdate(tamu);
	}

	@Override
	public Tamu findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		//get the employee
		Tamu tamu = currentSession.get(Tamu.class, id);
		
		//return employee
		return tamu;
	}

	@Override
	public void deleteById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<?> theQuery = currentSession.createQuery("delete from Tamu where id=:guestId");
		theQuery.setParameter("guestId", id);
		
		theQuery.executeUpdate();
		
	}

}
