package com.example.springboot.bukutamu.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="tb_tamu")
public class Tamu {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="nama")
	private String nama;
	
	@Column(name="no_ktp")
	private String no_ktp;
	
	@Column(name="foto", nullable = true, length = 64)
	private String foto;
	
	@Column(name="keperluan")
	private String keperluan;
	
	@Column(name="kesiapa")
	private String kesiapa;
	
	@Column(name="alamat")
	private String alamat;
	
	@Column(name="jenis_kelamin")
	private String jenis_kelamin;
	
	@Column(name="tanggal")
	private String tanggal;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNo_ktp() {
		return no_ktp;
	}
	public void setNo_ktp(String no_ktp) {
		this.no_ktp = no_ktp;
	}
	public String getFoto() {
		return foto;
	}
	
	@Transient
	public String getFotoImagePath() {
		if(foto == null || id == 0) return null;
		
		return "/tamu-foto/" + id + "/" + foto;
	}
	
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public String getKeperluan() {
		return keperluan;
	}
	public void setKeperluan(String keperluan) {
		this.keperluan = keperluan;
	}
	public String getKesiapa() {
		return kesiapa;
	}
	public void setKesiapa(String kesiapa) {
		this.kesiapa = kesiapa;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getJenis_kelamin() {
		return jenis_kelamin;
	}
	public void setJenis_kelamin(String jenis_kelamin) {
		this.jenis_kelamin = jenis_kelamin;
	}
	public String getTanggal() {
		return tanggal;
	}
	public void setTanggal(String tanggal) {
		this.tanggal = tanggal;
	}
	public Tamu() {
		
	}
	public Tamu(String nama, String no_ktp, String foto, String keperluan, String kesiapa, String alamat,
			String jenis_kelamin, String tanggal) {
		this.nama = nama;
		this.no_ktp = no_ktp;
		this.foto = foto;
		this.keperluan = keperluan;
		this.kesiapa = kesiapa;
		this.alamat = alamat;
		this.jenis_kelamin = jenis_kelamin;
		this.tanggal = tanggal;
	}
	
	
	
}
