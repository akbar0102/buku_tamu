package com.example.springboot.bukutamu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BukutamuApplication {

	public static void main(String[] args) {
		SpringApplication.run(BukutamuApplication.class, args);
	}

}
