package com.example.springboot.bukutamu.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.example.springboot.bukutamu.model.Tamu;
import com.example.springboot.bukutamu.service.TamuService;
import com.example.springboot.bukutamu.util.FileUploadUtil;

@Controller
public class TamuController {
	
	private TamuService tamuService;
	
	@Autowired
	public TamuController(TamuService tamuService) {
		this.tamuService = tamuService;
	}
	
	@GetMapping("/")
	public String viewHomePage(Model model) {
		model.addAttribute("listGuests", tamuService.findAll());
		
		return "index";
	}
	
	@GetMapping("/showNewGuestForm")
	public String showNewGuestForm(Model model) {
		Tamu tamu = new Tamu();
		model.addAttribute("guest", tamu);
		return "new_guest";
	}
	
	@PostMapping("/saveGuest")
	public String saveGuest(@ModelAttribute("guest") Tamu tamu, @RequestParam("image") MultipartFile multipartFile) {
		
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		System.out.println(fileName);
		tamu.setFoto(fileName);
		tamuService.save(tamu);
		
		String uploadDir = "tamu-foto/" + tamu.getId();
		
		try {
			FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/";
	}
	
	@GetMapping("/showFormForUpdate/{id}")
	public String showFormForUpdate(@PathVariable(value="id") int id, Model model) {
		Tamu tamu = tamuService.findById(id);
		
		if(tamu == null) {
			throw new RuntimeException("Tamu is not found - " + id);
		}
		
		model.addAttribute("guest", tamu);
		return "update_guest";
	}
	
	@GetMapping("/deleteGuest/{id}")
	public String deleteGuest(@PathVariable(value="id") int id, Model model) {
		Tamu tamu = tamuService.findById(id);
		
		if(tamu == null) {
			throw new RuntimeException("Tamu is not found - " + id);
		}
		
		tamuService.deleteById(id);
		return "redirect:/";
	}
}
