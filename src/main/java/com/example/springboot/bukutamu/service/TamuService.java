package com.example.springboot.bukutamu.service;

import java.util.List;

import com.example.springboot.bukutamu.model.Tamu;

public interface TamuService {
	public List<Tamu> findAll();
	
	public void save(Tamu tamu);
	
	public Tamu findById(int id);
	
	public void deleteById(int id);
}
