package com.example.springboot.bukutamu.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.springboot.bukutamu.model.Tamu;
import com.example.springboot.bukutamu.repository.TamuDAO;

@Service
public class TamuServiceImpl implements TamuService {
	
	private TamuDAO tamuDAO;
	
	@Autowired
	public TamuServiceImpl(TamuDAO tamuDAO) {
		this.tamuDAO = tamuDAO;
	}

	@Override
	@Transactional
	public List<Tamu> findAll() {
		// TODO Auto-generated method stub
		return tamuDAO.findAll();
	}

	@Override
	@Transactional
	public void save(Tamu tamu) {
		// TODO Auto-generated method stub
		tamuDAO.save(tamu);
	}

	@Override
	@Transactional
	public Tamu findById(int id) {
		// TODO Auto-generated method stub
		return tamuDAO.findById(id);
	}

	@Override
	@Transactional
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		tamuDAO.deleteById(id);
	}

}
